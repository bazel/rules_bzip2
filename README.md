# `rules_bzip2`

A Bazel ruleset to perform Burrows–Wheeler compression and decompression.

## Getting Started

Add the following to `MODULE.bazel`:

```py
bazel_dep(name="rules_bzip2", version="0.0.0")
```

Use the compression and decompression rules:

```py
load("@rules_bzip2//bzip2/compress:defs.bzl", "bzip2_compress")
load("@rules_bzip2//bzip2/decompress:defs.bzl", "bzip2_decompress")

bzip2_compress(
    name = "compress",
    src = "//fixture:hello-world.txt",
)

bzip2_decompress(
    name = "decompress",
    src = ":compress",
)
```

# Hermeticity

The module is entirely hermetic, using `@ape//ape/toolchain/info:bzip2` as the default toolchain.

The `@bzip2//:bzip2` is available as a toolchain but is only hermetic if a hermetic C/C++ toolchain
is configured. Select it with `--extra_toolchains=@rules_bzip2//bzip2/toolchain/bzip2:built`.

## Release Registry

The project publishes the relevant files to GitLab releases for use when a version has not been added to the upstream [BCR][bcr].

This is often the case for pre-release versions.

Add the following to `.bazelrc`:

```
# `bzlmod` pre-release registries
common --registry https://bcr.bazel.build
common --registry=https://gitlab.arm.com/bazel/rules_bzip2/-/releases/v1.0.0-alpha.1/downloads
```

Then a GitLab release version can be used in `bazel_dep`.

[bcr]: https://registry.bazel.build/
