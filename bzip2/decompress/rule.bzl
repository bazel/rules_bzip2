visibility("//bzip2/...")

DOC = """A Burrows-Wheeler compressed file to be decompressed."

```py
bzip2_decompress(
    name = "decompress",
    src = ":archive.bz2",
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "Decompresses a Burrows-Wheeler compressed file.",
        mandatory = True,
        allow_single_file = [".bz2"],
    ),
    "_template": attr.label(
        default = "@rules_coreutils//coreutils/redirect:stdout",
        allow_single_file = True,
    ),
}

def implementation(ctx):
    bzip2 = ctx.toolchains["//bzip2/toolchain/bzip2:type"]

    output = ctx.actions.declare_file(ctx.file.src.basename.removesuffix(".bz2"))
    rendered = ctx.actions.declare_file("{}.{}".format(ctx.label.name, ctx.file._template.extension))

    ctx.actions.expand_template(
        template = ctx.file._template,
        output = rendered,
        is_executable = True,
        substitutions = {
            "{{stdout}}": output.path,
        },
    )

    args = ctx.actions.args()
    args.add(bzip2.executable.path)
    args.add("-cdfk").add(ctx.file.src)

    ctx.actions.run(
        outputs = [output],
        inputs = [ctx.file.src],
        arguments = [args],
        tools = [bzip2.run],
        executable = rendered,
        toolchain = "//bzip2/toolchain/bzip2:type",
        mnemonic = "Bzip2Decompress",
        progress_message = "Decompressing %{input} to %{output}",
    )

    return DefaultInfo(files = depset([output]))

bzip2_decompress = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//bzip2/toolchain/bzip2:type"],
)

decompress = bzip2_decompress
