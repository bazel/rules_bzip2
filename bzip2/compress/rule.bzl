visibility("//bzip2/...")

DOC = """Compresses a file with the Burrows-Wheeler (`bzip2`) algorithm.

```py
bzip2_compress(
    name = "compress",
    src = "some/file.txt",
    level = 1-9,
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "A file to compress with the Burrows-Wheeler algorithm.",
        mandatory = True,
        allow_single_file = True,
    ),
    "level": attr.int(
        doc = "The compression level.",
        mandatory = False,
        default = 5,
        values = [l for l in range(1, 10)],
    ),
    "_template": attr.label(
        default = "@rules_coreutils//coreutils/redirect:stdout",
        allow_single_file = True,
    ),
}

def implementation(ctx):
    bzip2 = ctx.toolchains["//bzip2/toolchain/bzip2:type"]

    output = ctx.actions.declare_file("{}.bz2".format(ctx.file.src.basename))
    rendered = ctx.actions.declare_file("{}.{}".format(ctx.label.name, ctx.file._template.extension))

    ctx.actions.expand_template(
        template = ctx.file._template,
        output = rendered,
        is_executable = True,
        substitutions = {
            "{{stdout}}": output.path,
        },
    )

    args = ctx.actions.args()
    args.add(bzip2.executable.path)
    args.add("-czfk").add(ctx.file.src)
    args.add("-{}".format(ctx.attr.level))

    ctx.actions.run(
        outputs = [output],
        inputs = [ctx.file.src],
        arguments = [args],
        tools = [bzip2.run],
        executable = rendered,
        toolchain = "//bzip2/toolchain/bzip2:type",
        mnemonic = "Bzip2Compress",
        progress_message = "Compressing %{input} to %{output}",
    )

    return DefaultInfo(files = depset([output]))

bzip2_compress = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//bzip2/toolchain/bzip2:type"],
)

compress = bzip2_compress
